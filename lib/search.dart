import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

const IconData map = const IconData(0xf38c,
    fontFamily: CupertinoIcons.iconFont,
    fontPackage: CupertinoIcons.iconFontPackage);

const IconData nearby = const IconData(0xf473,
    fontFamily: CupertinoIcons.iconFont,
    fontPackage: CupertinoIcons.iconFontPackage);

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  Completer<GoogleMapController> _controller = Completer();
  MapType _currentMapType = MapType.normal;
  double latitude = 0.0;
  double longitude = 0.0;

  static final CameraPosition _kInit = CameraPosition(
    target: LatLng(0, 0),
    zoom: 1,
  );

  final Set<Marker> _markers = {};

  @override
  void initState() {
    super.initState();
    CurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    var satelliteButton = Padding(
        padding: const EdgeInsets.only(bottom: 8.0),
        child: CupertinoButton(
          minSize: 50,
          child: Column(
            children: <Widget>[
              Icon(
                map,
                color: Color.fromRGBO(88, 88, 88, 1),
              ),
              Text(
                'Satellite',
                style: TextStyle(
                    fontSize: 12, color: Color.fromRGBO(88, 88, 88, 1)),
              ),
            ],
          ),
          onPressed: _onMapTypeButtonPressed,
          color: Color.fromRGBO(254, 254, 254, 1),
          borderRadius: const BorderRadius.all(
            Radius.circular(2.0),
          ),
          padding: const EdgeInsets.all(5.0),
        ));

    var drawButton = Padding(
        padding: const EdgeInsets.only(bottom: 8.0),
        child: CupertinoButton(
          minSize: 50,
          child: Column(
            children: <Widget>[
              Icon(
                CupertinoIcons.pencil,
                color: Color.fromRGBO(88, 88, 88, 1),
              ),
              Text(
                'Draw',
                style: TextStyle(
                    fontSize: 12, color: Color.fromRGBO(88, 88, 88, 1)),
              ),
            ],
          ),
          onPressed: _onMapTypeButtonPressed,
          color: Color.fromRGBO(254, 254, 254, 1),
          borderRadius: const BorderRadius.all(
            Radius.circular(2.0),
          ),
          padding: const EdgeInsets.all(5.0),
        ));

    var nearbyButton = Padding(
        padding: const EdgeInsets.only(bottom: 8.0),
        child: CupertinoButton(
          minSize: 50,
          child: Column(
            children: <Widget>[
              Icon(
                nearby,
                color: Color.fromRGBO(88, 88, 88, 1),
              ),
              Text(
                'Nearby',
                style: TextStyle(
                    fontSize: 12, color: Color.fromRGBO(88, 88, 88, 1)),
              ),
            ],
          ),
          onPressed: _onMapTypeButtonPressed,
          color: Color.fromRGBO(254, 254, 254, 1),
          borderRadius: const BorderRadius.all(
            Radius.circular(2.0),
          ),
          padding: const EdgeInsets.all(5.0),
        ));
    return CupertinoPageScaffold(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(CupertinoIcons.search),
              ),
              Expanded(
                child: CupertinoTextField(),
              ),
              CupertinoButton(
                child: Text('List'),
              )
            ],
          ),
          Expanded(
            child: Stack(
              children: <Widget>[
                GoogleMap(
                  initialCameraPosition: _kInit,
                  mapType: _currentMapType,
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                  markers: _markers,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: Column(
                      children: <Widget>[
                        satelliteButton,
                        drawButton,
                        nearbyButton,
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  void CurrentLocation() async {
    Position _position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print(_position);
    latitude = _position.latitude;
    longitude = _position.longitude;

    LatLng _center = LatLng(latitude, longitude);

    CameraPosition _kUpdate =
        CameraPosition(target: LatLng(latitude, longitude), zoom: 13);
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kUpdate));

    setState(() {
      _markers.add(Marker(
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId(_center.toString()),
        position: _center,
        icon: BitmapDescriptor.defaultMarker,
      ));
    });
  }

  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }
}
