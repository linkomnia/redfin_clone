import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyRedfinTabPage extends StatefulWidget {
  @override
  _MyRedfinTabPageState createState() => _MyRedfinTabPageState();
}

class _MyRedfinTabPageState extends State<MyRedfinTabPage> {
  @override
  Widget build(BuildContext context) {
    var facebookButton = Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 20, right: 20),
      child: SizedBox(
        width: double.infinity,
        child: CupertinoButton(
          padding: const EdgeInsets.only(top: 0, bottom: 0),
          borderRadius: const BorderRadius.all(Radius.circular(2.0)),
          child: Row(
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.only(left: 70.0, right: 20.0),
                  child: Icon(FontAwesomeIcons.facebookSquare)),
              Expanded(child: Text('Continue with Facebook')),
            ],
          ),
          color: Color.fromRGBO(76, 111, 169, 1),
          onPressed: () {
            // Navigator.of(context).pushNamed('/search');
          },
        ),
      ),
    );

    var googleButton = Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 20, right: 20),
      child: SizedBox(
        width: double.infinity,
        child: CupertinoButton(
          padding: const EdgeInsets.only(top: 0, bottom: 0),
          borderRadius: const BorderRadius.all(Radius.circular(2.0)),
          child: Row(
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.only(left: 70.0, right: 20.0),
                  child: Icon(FontAwesomeIcons.google)),
              Expanded(child: Text('Continue with Google')),
            ],
          ),
          color: Color.fromRGBO(81, 154, 246, 1),
          onPressed: () {
            // Navigator.of(context).pushNamed('/search');
          },
        ),
      ),
    );

    var emailButton = Padding(
      padding: const EdgeInsets.only(top: 3.0, left: 20, right: 20),
      child: SizedBox(
        width: double.infinity,
        child: OutlineButton(
          child: Text(
            'Continue with Email',
            style: TextStyle(color: Color.fromRGBO(100, 100, 100, 1)),
          ),
          color: Color.fromRGBO(255, 255, 255, 1),
          onPressed: () {
            // Navigator.of(context).pushNamed('/search');
          },
        ),
      ),
    );

    var signInButton = Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 20, right: 20),
      child: SizedBox(
        width: double.infinity,
        child: CupertinoButton(
          padding: const EdgeInsets.only(top: 0, bottom: 0),
          borderRadius: const BorderRadius.all(Radius.circular(2.0)),
          child: Text('Sign In'),
          onPressed: () {
            // Navigator.of(context).pushNamed('/search');
          },
        ),
      ),
    );

    return CupertinoPageScaffold(
      backgroundColor: Color.fromRGBO(254, 254, 254, 1),
      navigationBar: CupertinoNavigationBar(
        middle: const Text('My Home'),
      ),
      child: Center(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 200.0),
              child: CupertinoButton(
                color: Color.fromRGBO(100, 100, 100, 0.2),
                padding: const EdgeInsets.only(
                    left: 10.0, right: 10.0, top: 8.0, bottom: 12.0),
                borderRadius: const BorderRadius.all(Radius.circular(50.0)),
                child: Icon(
                  CupertinoIcons.home,
                  size: 45,
                  color: Color.fromRGBO(100, 100, 100, 1),
                ),
                onPressed: () {},
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: SizedBox(
                width: 200,
                child: Text(
                  'Join to unlock the full experience',
                  style: TextStyle(fontSize: 22.0),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Expanded(
              child: Container(),
            ),
            facebookButton,
            googleButton,
            emailButton,
            signInButton,
            Text(
              'By signing up you agree to our Terms of Use and Privacy Policy.',
              style: TextStyle(
                  color: Color.fromRGBO(200, 200, 200, 1), fontSize: 12),
            ),
            SizedBox(
              height: 60,
              child: Container(),
            ),
          ],
        ),
      ),
    );
  }
}
