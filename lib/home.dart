import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:redfin_clone/my_home.dart';
import 'package:redfin_clone/my_redfin.dart';
import 'package:redfin_clone/new_home.dart';
import 'package:redfin_clone/search.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.search),
            title: Text('Find Homes'),
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.bell),
            title: Text('New Homes'),
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.home),
            title: Text('My Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.person),
            title: Text('My Redfin'),
          ),
        ],
      ),
      tabBuilder: (context, index) {
        return CupertinoTabView(
            routes: {
              "/search": (BuildContext context) => SearchPage(),
              "/new_home": (BuildContext context) => NewHomesTabPage(),
              "/my_home": (BuildContext context) => MyHomeTabPage(),
              "/my_redfin": (BuildContext context) => MyRedfinTabPage(),
            },
            builder: (context) {
              switch (index) {
                case 0:
                  return SearchPage();
                case 1:
                  return NewHomesTabPage();
                case 2:
                  return MyHomeTabPage();
                case 3:
                  return MyRedfinTabPage();
              }
            });
      },
    );
  }
}
