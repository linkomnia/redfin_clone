import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:redfin_clone/search.dart';

class NewHomesTabPage extends StatefulWidget {
  @override
  _NewHomesTabPageState createState() => _NewHomesTabPageState();
}

class _NewHomesTabPageState extends State<NewHomesTabPage> {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: Color.fromRGBO(254, 254, 254, 1),
      navigationBar: CupertinoNavigationBar(
        middle: const Text('New Homes For You'),
      ),
      child: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 100.0, bottom: 25.0),
              child: Icon(
                CupertinoIcons.bell,
                size: 100.0,
                color: Color.fromRGBO(100, 100, 100, 0.2),
              ),
            ),
            Text(
              'No recommendations found',
              style: TextStyle(fontSize: 22.0),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 25.0,
              ),
              child: SizedBox(
                width: 300,
                child: Text(
                  'Get more personalized recommendations as you favorite more homes.',
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: 40.0, left: 25.0, right: 25.0),
              child: SizedBox(
                width: double.infinity,
                child: CupertinoButton(
                  borderRadius: const BorderRadius.all(Radius.circular(2.0)),
                  child: Text('Start Home Search'),
                  color: Color.fromRGBO(183, 51, 43, 1),
                  onPressed: () {
                    // Navigator.of(context).pushNamed('/search');
                    Navigator.push(
                        context,
                        CupertinoPageRoute(
                            builder: (context) => CupertinoTabView(
                                  builder: (context) {
                                    return SearchPage();
                                  },
                                )));
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
