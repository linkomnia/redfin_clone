import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:redfin_clone/search.dart';

class MyHomeTabPage extends StatefulWidget {
  @override
  _MyHomeTabPageState createState() => _MyHomeTabPageState();
}

class _MyHomeTabPageState extends State<MyHomeTabPage> {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: Color.fromRGBO(254, 254, 254, 1),
      navigationBar: CupertinoNavigationBar(
        middle: const Text('My Home'),
      ),
      child: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 130.0, bottom: 25.0),
              child: Image.asset('assets/images/my_home.png'),
            ),
            Text(
              'Do you own a home?',
              style: TextStyle(fontSize: 22.0),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 15.0,
              ),
              child: SizedBox(
                width: 250,
                child: Text(
                  'Track your home\'s value and nearby sales.',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Color.fromRGBO(88, 88, 88, 0.3)),
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: 40.0, left: 25.0, right: 25.0),
              child: SizedBox(
                width: double.infinity,
                child: CupertinoButton(
                  borderRadius: const BorderRadius.all(Radius.circular(2.0)),
                  child: Text('Claim My Home'),
                  color: Color.fromRGBO(183, 51, 43, 1),
                  onPressed: () {},
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
