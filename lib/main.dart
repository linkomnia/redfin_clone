import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:redfin_clone/home.dart';

void main() => runApp(MyApp());

final routes = {
  "/home": (BuildContext context) => HomePage(),
};

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      debugShowCheckedModeBanner: false,
      home: LoadingPage(),
      routes: routes,
    );
  }
}

class LoadingPage extends StatefulWidget {
  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  @override
  void initState() {
    super.initState();

    new Future.delayed(Duration(seconds: 3), () async {
      print('loading App');
      Navigator.pushReplacementNamed(context, "/home");
    });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 18.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(),
            ),
            Expanded(
              child: Container(
                child: Column(
                  children: <Widget>[
                    Image.asset('assets/images/splash.png'),
                    Text(
                      'Full-service agents. As low as a 1% listing fee.',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Text(
              '1% listing fee not available in all markets.',
              style: TextStyle(
                fontSize: 14,
                color: Color.fromRGBO(0, 0, 0, 0.6),
              ),
            ),
            Text(
              'Minimums and other terms apply.',
              style: TextStyle(
                fontSize: 14,
                color: Color.fromRGBO(0, 0, 0, 0.6),
              ),
            )
          ],
        ),
      ),
    );
  }
}
